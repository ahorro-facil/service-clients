/*

    Las pruebas se realizarón mediante POSTMAN

    *** Para realizar un POST
        {

            "name": "Nelson Paltin",
            "amount": 15000.0,
            "accountId": (1,2,3,4),
            "autoRecomendation": true
        }
    *** Para realizar un GET

        localhost:8080/user/account?accountId= (y el número de cuenta)
*/



package com.ahorro.facil.UserAccounts.modelos;

import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "user_id")
    private long userId;


    @Column(name = "autoRecomendation")
	private boolean autoRecomendation;

    public Account(String name, double amount, long userId, boolean autoRecomendation) {
        this.name = name;
        this.amount = amount;
        this.userId = userId;
        this.autoRecomendation = autoRecomendation;
    }

}

/*


Para realizar un POST
{
  "title": "cuenta 1",
  "amount": 15000,
  "userId": 1,
  "active": true
}

Para realizar un GET

localhost:8080/user/incomes?id=1

*/
