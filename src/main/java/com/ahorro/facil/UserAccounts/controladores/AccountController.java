package com.ahorro.facil.UserAccounts.controladores;

import java.util.List;

import com.ahorro.facil.UserAccounts.modelos.Account;
import com.ahorro.facil.UserAccounts.repositorios.AccountRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class AccountController {

    @Autowired
    AccountRepository accountRepository;


    @GetMapping("/account")
    public ResponseEntity<List<Account>> findAccountByAccountId(@RequestParam(required = true) long id) {
        
        
        
        List<Account> account = accountRepository.findByUserId(id);


        return new ResponseEntity<>(account, HttpStatus.OK);
    }



    @PostMapping("/account")
    public ResponseEntity<Account> registerConfiguration(@RequestBody Account acc) {
        try {
            Account created = accountRepository.save(
                    new Account(acc.getName(), acc.getAmount(),acc.getUserId(), acc.isAutoRecomendation()));
            return new ResponseEntity<>(created, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/prueba")
    public String saludo(){
        return "Prueba de configuracion de cuentas de usuario";
    }
}