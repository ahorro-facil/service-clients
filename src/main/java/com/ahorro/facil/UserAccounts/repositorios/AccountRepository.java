package com.ahorro.facil.UserAccounts.repositorios;

import java.util.List;

import com.ahorro.facil.UserAccounts.modelos.Account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AccountRepository extends JpaRepository<Account, Long>{
    
     
    @Query(
        value = "SELECT * FROM  account i WHERE i.user_id= ?",
        nativeQuery = true
    )List<Account> findByUserId(long userId);
   
}